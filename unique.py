input_name = input("Enter the name of your text file.")

input_file = open(input_name, 'r')
text = input_file.read()
words = text.split()
input_file.close()
unique = set(words)
print('These are the unique words in the text:')
for word in unique:
    print(word)

unique_dict = {}
for word in words:
    if word in unique_dict:
        unique_dict[word] += 1
    else:
        unique_dict[word] = 1

    

