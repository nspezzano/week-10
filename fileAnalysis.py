input_name = input("Enter the name of your text file.")

input_file = open(input_name, 'r')
text = input_file.read()
words = text.split()
input_file.close()
unique = set(words)
print('These are the unique words in text file 1:')
for word in unique:
    print(word)

unique_dict = {}
for word in words:
    if word in unique_dict:
        unique_dict[word] += 1
    else:
        unique_dict[word] = 1

input_name = input("Enter the name of your text file.")

input_file = open(input_name, 'r')
text = input_file.read()
words2 = text.split()
input_file.close()
unique = set(words2)
print('These are the unique words in text file 2:')
for word in unique:
    print(word)

unique_dict = {}
for word in words2:
    if word in unique_dict:
        unique_dict[word] += 1
    else:
        unique_dict[word] = 1
   
list1 = words
list2 = words2
w = list(set(list1).intersection(list2))
print('')
print("Words in both text files", w)
print('')
x = set(list1) - set(list2)
print('')
print("Words in the first file but not in the second", x)
print('')
y = set(list2) - set(list1)
print('')
print("Words in the second file but not in the first", y)
z = x - y
print('')
print("Words that are in the either the first or second file but not both.", z)

